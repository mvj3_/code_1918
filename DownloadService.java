

	public void onCreate() {
		super.onCreate();
		fileService = new FileService();
		flowData = new FlowData();
		fileSaveDir = Environment.getExternalStorageDirectory();
		if (!fileSaveDir.exists())
			fileSaveDir.mkdirs();

	}

	public int onStartCommand(final Intent intent, int flags, int startId) {
		// setForeground(true);
		myHandler = new MyHandler();

		if (intent != null) {
			bundle = intent.getExtras();
			url = bundle.getString("down");
			time = bundle.getInt("time");
			flow = bundle.getInt("flow");
			max = bundle.getInt("max");
			// Log.i("dayin DownloadService  xia  url  ====== ", url);
			/*
			 * Log.i("dayin textDatas ===========  ", "time = " + time +
			 * ",flow = " + flow + ",max = " + max);
			 */
			receiveTotal = WebIeSubPage.wifis();
		}

		if (downthread != null) {
			downthread = null;
			downthread = new TestThread();
			downthread.start();
			flag = true;
		} else {
			downthread = new TestThread();
			downthread.start();
			flag = true;
		}
		myHandler.sendEmptyMessageDelayed(3, 1000);

		dt = new Date();
		mTime = dt.getTime();
		return Service.START_NOT_STICKY;
	}

	class MyHandler extends Handler {

		public MyHandler() {

		}

		public MyHandler(Looper L) {

			super(L);

		}

		// 子类必须重写此方法,接受数据

		@Override
		public void handleMessage(Message msg) {
			receiveTotal1 = WebIeSubPage.wifis();
			d = receiveTotal1 - receiveTotal;
			Log.i("dayin dddd =========== ", d + "");
			flowData.setFlowd(d);
			fileService.saveData(flowData);
			long dingTime = System.currentTimeMillis() - mTime;
			if (dingTime < 0) {
				return;
			}
			if (time > 0 && (time * 60 * 1000) <= dingTime) {
				if (downthread != null) {
					flag = false;

					downthread.interrupt();
					Log.e("status1 time", downthread.isInterrupted() + "");

					flowData.setFlowd(d);
					fileService.saveData(flowData);
				}
			} else if (flow > 0 && flow <= d) {
				if (downthread != null) {
					flag = false;
					downthread.interrupt();
					Log.e("status2 flow", downthread.isInterrupted() + "");

					flowData.setFlowd(d);
					fileService.saveData(flowData);
				}
			} else if (max1 == 1) {
				max1 = 0;
				flag = false;
				downthread.interrupt();
				Log.e("status3 max", downthread.isInterrupted() + "");

				flowData.setFlowd(d);
				fileService.saveData(flowData);
			} else {

				myHandler.sendEmptyMessageDelayed(3, 1000);
			}

		}

	}

