
		monitorBatteryState();
		Timer nT1 = new Timer();
		nT1.schedule(new TimerTask() { // 计划运行时间间隔
					public void run() {
						Message message = Message.obtain();
						message.obj = new MessageHandler.HandlerMission() {
							public void exec() {
								Date dt = new Date();
								SimpleDateFormat sdf = new SimpleDateFormat(
										"yyyy-MM-dd HH:mm:ss aa");
								String temp = sdf.format(dt);
								temp = temp.substring(11, 16);
								timeView.setText(temp);
							}
						};
						messageHandler.sendMessage(message);
					}
				}, 0, 1000);

		ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState();
		State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.getState();

		bundle = this.getIntent().getExtras();
		final String name = bundle.getString("name");
		taname.setText(name);
		WindowManager windowManager = getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		width = display.getWidth();
		height = display.getHeight();

		// 初始化代理服务器// 预加载视频文件存放路径, 预加载体积,预加载文件上限
		// proxy = new HttpGetProxy(getBufferDir(), PREBUFFER_SIZE, 10);
		// ids = System.currentTimeMillis() + "";

		if (wifi == State.CONNECTED) {
			url = bundle.getString("hlitpic");
			uri = Uri.parse(url);
			/*
			 * try { proxy.startDownload(ids, url, true); } catch (Exception e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 * delayToStartPlay.sendEmptyMessageDelayed(0, waittime);
			 */
			// vv.setVideoPath(url);

		} else if (mobile == State.CONNECTED) {
			url = bundle.getString("litpic");
			/*
			 * APNUtil apnUtil = new APNUtil(this); int type =
			 * apnUtil.getNetWorkType(); Log.i("========== type \\\\\\\\",
			 * type+""); if (type == APNUtil.NET_TYPE_WAP) {
			 * vv.setVideoPath(url);
			 * 
			 * } else { uri = Uri.parse(url); }
			 */
			uri = Uri.parse(url);
			// String subUrl = url.substring(0, 4);

		}

	

		

		
		bn3.setAlpha(0xBB);
		bn4.setAlpha(0xBB);
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		currentVolume = mAudioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		soundBar.setMax(maxVolume);
		soundBar.setProgress(currentVolume);
		soundBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() // 调音监听器
		{
			public void onProgressChanged(SeekBar arg0, int progress,
					boolean fromUser) {
				mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
						progress, 0);
				currentVolume = mAudioManager
						.getStreamVolume(AudioManager.STREAM_MUSIC); // 获取当前值
				soundBar.setProgress(currentVolume);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub

			}
		});

	

		bn3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cancelDelayHide();// 取消隐藏延迟
				if (isPaused) {
					vv.start();
					bn3.setImageResource(R.drawable.play_btn_pause);
					hideControllerDelay();// 延迟隐藏控制器
				} else {
					vv.pause();
					bn3.setImageResource(R.drawable.play_btn_player);
				}
				isPaused = !isPaused;

			}

		});

		back_bnt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				VideoPlayerPage.this.finish();
			}
		});

		bn5.setImageResource(R.drawable.play_btn_volume);
		bn5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isSilent) {
					bn5.setImageResource(R.drawable.play_btn_volume);
				} else {
					bn5.setImageResource(R.drawable.play_ctrl_metu);
				}
				isSilent = !isSilent;
				updateVolume(currentVolume);
				cancelDelayHide();// // 取消隐藏延迟
				hideControllerDelay();// 延迟隐藏控制器
			}
		});
		// 屏幕大小变化

		dxh.setImageResource(R.drawable.play_btn_tomini);
		dxh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isFullScreen) {
					dxh.setImageResource(R.drawable.play_btn_tomini);
					setVideoScale(SCREEN_DEFAULT);// 设置视频显示尺寸
				} else {
					dxh.setImageResource(R.drawable.play_ctrl_fullsreen_selectedfu);

					setVideoScale(SCREEN_FULL);// 设置视频显示尺寸
				}
				bn3.setImageResource(R.drawable.play_btn_pause);
				isFullScreen = !isFullScreen;
			}
		});

		seekBar = (SeekBar) controlView.findViewById(R.id.seekbar);
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekbar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub

				if (fromUser) {
					vv.seekTo(progress);// 设置播放位置
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				myHandler.removeMessages(HIDE_CONTROLER);
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				myHandler.sendEmptyMessageDelayed(HIDE_CONTROLER, TIME);
			}
		});

		getScreenSize();// 获得屏幕尺寸大小
		mGestureDetector = new GestureDetector(new SimpleOnGestureListener() {

			@Override
			public boolean onDoubleTap(MotionEvent e) {
				// TODO Auto-generated method stub
				if (isFullScreen) {
					dxh.setImageResource(R.drawable.play_btn_tomini);
					setVideoScale(SCREEN_DEFAULT);// 设置视频显示尺寸
				} else {
					dxh.setImageResource(R.drawable.play_ctrl_fullsreen_selectedfu);

					setVideoScale(SCREEN_FULL);// 设置视频显示尺寸
				}
				bn3.setImageResource(R.drawable.play_btn_pause);
				isFullScreen = !isFullScreen;
				// Log.d(TAG, "onDoubleTap");

				if (isControllerShow) {
					showController();// 显示控制器
					showControllert();
					showmSoundWindow();
				}
				// return super.onDoubleTap(e);
				return true;
			}

			@Override
			public boolean onSingleTapConfirmed(MotionEvent e) {// 轻击屏幕
				// TODO Auto-generated method stub
				if (!isControllerShow) {// 是否显示控制器
					showController();// 显示控制器
					showControllert();
					showmSoundWindow();
					hideControllerDelay();// 延迟隐藏控制器
				} else {
					cancelDelayHide();// 取消隐藏延迟
					hideController();// 隐藏控制器
					hidemSoundWindow();
					hideControllert();
				}
				// return super.onSingleTapConfirmed(e);
				return true;
			}

			@Override
			public void onLongPress(MotionEvent e) {// 长按屏幕
				// TODO Auto-generated method stub
				if (isPaused) {
					vv.start();
					bn3.setImageResource(R.drawable.play_btn_pause);
					cancelDelayHide();// 取消隐藏延迟
					hideControllerDelay();// 延迟隐藏控制器
				} else {
					vv.pause();
					bn3.setImageResource(R.drawable.play_btn_player);
					cancelDelayHide();// 取消隐藏延迟
					showController();// 显示控制器
					showControllert();
					showmSoundWindow();
				}
				isPaused = !isPaused;
				// super.onLongPress(e);
			}
		});

		vv.setOnPreparedListener(new OnPreparedListener() {// 注册在媒体文件加载完毕，可以播放时调用的回调函数

			@Override
			public void onPrepared(MediaPlayer arg0) {// 加载
				setVideoScale(SCREEN_DEFAULT);
				isFullScreen = false;
				if (isControllerShow) {
					showController();// 显示控制器
					showControllert();
					showmSoundWindow();
				}

				int i = vv.getDuration();// 获得所播放视频的总时间
				// Log.d("onCompletion", "" + i);
				seekBar.setMax(i);
				i /= 1000;
				int minute = i / 60;
				int hour = minute / 60;
				int second = i % 60;
				minute %= 60;
				durationTextView.setText(String.format("%02d:%02d:%02d", hour,
						minute, second));
				vv.start();
				// long duration=System.currentTimeMillis() - startTimeMills;
				Looper.myQueue().addIdleHandler(new IdleHandler() {
					@Override
					public boolean queueIdle() {// 空闲的队列

						if (controler != null && controlert != null
								&& vv.isShown()) {// controler控制器(PopupWindow)
							controler.showAtLocation(vv, Gravity.BOTTOM, 0, 0);// 屏幕底部显示控制器
							controlert.showAtLocation(vv, Gravity.TOP, 0, 0);
							mSoundWindow.showAtLocation(vv, Gravity.RIGHT
									| Gravity.TOP, 0, -100);// |
															// Gravity.CENTER_VERTICAL
							controler.update(0, 0, screenWidth, controlHeight);// 控制器宽、高
							controlert.update(0, 0, screenWidth, controlHeight);// 控制器宽、高
							mSoundWindow.update(0, -500, 100, 1000);
						}

						return false;
					}
				});
				bn3.setImageResource(R.drawable.play_btn_pause);
				hideControllerDelay();// 延迟隐藏控制器
				myHandler.sendEmptyMessage(PROGRESS_CHANGED);
				pb.setVisibility(View.GONE);
				tv.setVisibility(View.GONE);
			}
		});

		vv.setOnCompletionListener(new OnCompletionListener() {// 注册在媒体文件播放完毕时调用的回调函数

			@Override
			public void onCompletion(MediaPlayer arg0) {

				vv.stopPlayback();
				VideoPlayerPage.this.finish();
			}
		});
	}

	/*
	 * private Handler delayToStartPlay = new Handler() { public void
	 * handleMessage(Message msg) { startTimeMills=System.currentTimeMillis();
	 * String proxyUrl = proxy.getLocalURL(ids); vv.setVideoPath(proxyUrl); } };
	 */
	
	