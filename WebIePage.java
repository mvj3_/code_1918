

public class WebIePage extends Activity {
	protected static final int CLOSE_DIA = 0;
	private WebView view;
	private ProgressBar progressBar;
	private ConnectivityManager conMan;
	private double wd;
	private FileService fileService;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		wd = WebIeSubPage.wifis();
		new Handler();
		conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
		conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
		view = (WebView) findViewById(R.id.browser_webview);
		init();
		view.post(new Runnable() {
			@Override
			public void run() {
				view.loadUrl("http://m.youku.com/smartphone/index");
				// view.loadUrl("http://www.csmap.gov.cn/imap_rest/services/xingzhengqu/MapServer/2acc48e6-8eda-4120-828e-7d4402f1e870");
				// http://m.youku.com/smartphone/index
				// http://m.qiyi.com/
				// http://m.letv.com/
				// http://m.cnr.cn
				// http://android.wo.com.cn/zn/index.jsp
				// http://m.hao.360.cn/simple/
				// http://m.hao123.com
				// http://wap.8448.cc/
				// http://192.168.20.134:8080/guanggao/jiekou!qidong
			}
		});

	}

	@SuppressLint("SetJavaScriptEnabled")
	private void init() {
		progressBar = (ProgressBar) findViewById(R.id.progressbar);
		progressBar.setMax(100);

		WebSettings ws = view.getSettings();
		ws.setPluginState(PluginState.ON);
		ws.setJavaScriptEnabled(true);
		ws.setPluginsEnabled(true);
		ws.setAllowFileAccess(true);
		ws.setBuiltInZoomControls(true); // 设置显示缩放按钮
		ws.setSupportZoom(true); // 支持缩放
		ws.setUseWideViewPort(true);

		view.setWebViewClient(new ToWebViewClient());
		view.setWebChromeClient(new ChromeClient());

		ws.setLayoutAlgorithm(LayoutAlgorithm.NARROW_COLUMNS);
		ws.setDefaultTextEncodingName("UTF-8");
		ws.setAppCacheEnabled(true);
		ws.setCacheMode(WebSettings.LOAD_DEFAULT);

	}

	protected void onResume() {
		super.onResume();
		// WebView.enablePlatformNotifications();
	}

	protected void onStop() {
		super.onStop();
		// WebView.disablePlatformNotifications();

	}

	protected void onDestroy() {
		super.onDestroy();
		double dw = WebIeSubPage.wifis();
		double dws = dw - wd;
		FlowData flowData = new FlowData();
		fileService = new FileService();
		flowData.setFlowz(dws);
		fileService.saveData(flowData);
		List<FlowData> flowdatas = fileService.getData();
		FlowData flowData2 = new FlowData();
		for (FlowData flow : flowdatas) {
			double z = flow.getFlowz();
			double y = flow.getFlowy();
			double p = flow.getFlowp();
			double d = flow.getFlowd();
			if (z > 0) {
				flowData2.setFlowz(z);
			} else if (y > 0) {
				flowData2.setFlowy(y);
			} else if (p > 0) {
				flowData2.setFlowp(p);
			} else if (d > 0) {
				flowData2.setFlowd(d);
			}
		}
		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm:ss");

		String temp = sdf.format(dt);
		flowData2.setDated(temp);
		fileService.delData();
		fileService.saveDatato(flowData2);
	}

	// Web视图
	private class ToWebViewClient extends WebViewClient {

		@Override
		public void onLoadResource(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onLoadResource(view, url);
			// System.out.println("onLoadResource:" + url);
			// onPageStarted(view, url, null);
			Log.i("打印得到的 onLoadResource = ", url);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.i("打印得到的 shouldOverrideUrlLoading ================== ", url);
			Intent intent = new Intent(WebIePage.this, WebIeSubPage.class);
			Bundle bundle = new Bundle();
			bundle.putString("WebUrl", url);
			intent.putExtras(bundle);
			startActivity(intent);

			return true;
		}

		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			view.loadUrl("file:///android_asset/pindao.png");
		}

	}

	private class ChromeClient extends WebChromeClient {
		@Override
		public boolean onJsAlert(WebView view, String url, String message,
				JsResult result) {
			return super.onJsAlert(view, url, message, result);
		}

		// JavaScript输入框
		@Override
		public boolean onJsPrompt(WebView view, String url, String message,
				String defaultValue, JsPromptResult result) {
			return super.onJsPrompt(view, url, message, defaultValue, result);
		}

		// JavaScript确认框
		@Override
		public boolean onJsConfirm(WebView view, String url, String message,
				JsResult result) {
			return super.onJsConfirm(view, url, message, result);
		}

		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			progressBar.setProgress(newProgress);
			super.onProgressChanged(view, newProgress);
		}

	}

}
